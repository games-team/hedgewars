Source: hedgewars
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Dmitry E. Oboukhov <unera@debian.org>,
           Gianfranco Costamagna <locutusofborg@debian.org>
Standards-Version: 4.7.0
Build-Depends: cmake,
               clang [armel armhf i386 mipsel ppc64el],
               debhelper-compat (= 13),
               fp-compiler [!armel !armhf !i386 !mipsel !ppc64el],
               fp-units-misc [!armel !armhf !i386 !mipsel !ppc64el],
               fp-units-gfx [!armel !armhf !i386 !mipsel !ppc64el],
               ghc,
# convert function
               imagemagick,
               libavcodec-dev,
               libavformat-dev,
               libghc-aeson-dev,
               libghc-entropy-dev,
               libghc-hslogger-dev,
               libghc-mtl-dev,
               libghc-network-dev,
               libghc-network-bsd-dev,
               libghc-parsec3-dev [armel armhf i386 mipsel ppc64el],
               libghc-random-dev,
               libghc-regex-tdfa-dev,
               libghc-sandi-dev,
               libghc-sha-dev,
               libghc-text-dev,
               libghc-utf8-string-dev,
               libghc-vector-dev,
               libghc-yaml-dev,
               libghc-zlib-dev,
               liblua5.1-dev,
               libphysfs-dev,
               libpng-dev,
               libsdl2-image-dev,
               libsdl2-mixer-dev,
               libsdl2-net-dev,
               libsdl2-ttf-dev,
               libsdl2-dev,
               qtbase5-dev,
               qtbase5-private-dev,
               qttools5-dev-tools,
               qttools5-dev,
# finding system fonts
               fonts-dejavu-core,
               fonts-wqy-zenhei
Homepage: https://hedgewars.org
Vcs-Browser: https://salsa.debian.org/games-team/hedgewars
Vcs-Git: https://salsa.debian.org/games-team/hedgewars.git
Rules-Requires-Root: no

Package: hedgewars
Architecture: any
Depends: hedgewars-data (>= ${source:Upstream-Version}),
         fonts-dejavu-core,
         fonts-wqy-zenhei,
         qt5-style-plugins,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Funny turn-based artillery game, featuring fighting hedgehogs!
 Each player controls a team of several hedgehogs. During the
 course of the game, players take turns with one of their
 hedgehogs. They then use whatever tools and weapons are
 available to attack and kill the opponents' hedgehogs, thereby
 winning the game. Hedgehogs may move around the terrain in a
 variety of ways, normally by walking and jumping but also by
 using particular tools such as the "Rope" or "Parachute", to
 move to otherwise inaccessible areas. Each turn is time-limited
 to ensure that players do not hold up the game with excessive
 thinking or moving.
 .
 A large variety of tools and weapons are available for players
 during the game: Grenade, Cluster Bomb, Bazooka, UFO, Homing Bee,
 Shotgun, Desert Eagle, Fire Punch, Baseball Bat, Dynamite, Mine,
 Rope, Pneumatic pick, Parachute. Most weapons, when used, cause
 explosions that deform the terrain, removing circular chunks.
 The landscape is an island floating on a body of water, or a
 restricted cave with water at the bottom. A hedgehog dies when
 it enters the water (either by falling off the island, or
 through a hole in the bottom of it), it is thrown off either
 side of the arena or when its health is reduced, typically from
 contact with explosions, to zero (the damage dealt to the
 attacked hedgehog or hedgehogs after a player's or CPU turn is
 shown only when all movement on the battlefield has ceased).

Package: hedgewars-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: hedgewars
Description: Data files for hedgewars
 This package contains data files for the hedgewars package.
 Examples of files are: maps, scripts, themes, images, sounds,
 level data and other miscellaneous files needed by hedgewars.
